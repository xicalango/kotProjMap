
Use a normal projector to map colors to keys on a keyboard.

Demos:

- <https://www.instagram.com/p/BbYHwEADCT4/>
- <https://www.instagram.com/p/BbToDKpD7TQ/>
- <https://www.instagram.com/p/BbTi5PgjOjY/>